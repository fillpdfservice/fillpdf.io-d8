<?php

namespace FillPdfService\composer;

use Symfony\Component\Filesystem\Filesystem;
use Composer\Script\Event;

/**
 * Composer script.
 */
class CustomHandler {

  /**
   * Symlink vendor/bin to ../bin.
   *
   * This fixes Drush not being detected properly.
   *
   * @param \Composer\Script\Event $event
   *   The event.
   */
  public static function createRequiredFiles(Event $event) {
    $fs = new Filesystem();

    // Ensure vendor/bin links to bin.
    $config = $event->getComposer()->getConfig();
    if ($config->get('bin-dir') !== ($config->get('vendor-dir') . '/bin')) {
      $fs->remove('./vendor/bin');
      // We hardcode this so it will work on host and guest. This is custom,
      // anyway.
      $fs->symlink('../bin', './vendor/bin');
    }
  }

}
