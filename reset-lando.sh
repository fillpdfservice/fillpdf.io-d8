#!/usr/bin/env bash
DB_USER=drupal8
DB_PASS=drupal8
DB_HOST=127.0.0.1
DB_PORT=`lando info | jq '.database.external_connection.port' | tr -d \"` # changes
DRUPAL_DB=drupal8
LEGACY_DB=legacy

echo "Detected database port: ${DB_PORT}"

cd /Users/kevin/vhosts/fillpdf-service/d7d8/drupal-vm
vagrant ssh -c "cd /var/www/drupalvm/web; drush sql-dump --result-file=/var/www/private/migratetestseed.sql"

cd /Users/kevin/vhosts/fillpdf-service/d7d8/docroot/web

lando drush sql-drop -y
lando mysql -e "CREATE DATABASE IF NOT EXISTS ${DRUPAL_DB}; GRANT ALL PRIVILEGES ON ${DRUPAL_DB}.* TO ${DB_USER}";
mysql -u ${DB_USER} --password=${DB_PASS} -h ${DB_HOST} -P ${DB_PORT} ${DRUPAL_DB} < ../../private/migratetestseed.sql 2>/dev/null

mysql -u ${DB_USER} --password=${DB_PASS} -h ${DB_HOST} -P ${DB_PORT} -e "DROP DATABASE ${LEGACY_DB}"; 2>/dev/null
lando mysql -e "CREATE DATABASE ${LEGACY_DB}; GRANT ALL PRIVILEGES ON ${LEGACY_DB}.* TO ${DB_USER}";
mysql -u ${DB_USER} --password=${DB_PASS} -h ${DB_HOST} -P ${DB_PORT} legacy < ../../private/db/drupal7.sql 2>/dev/null

lando drush cr

echo "Databases reset!"
