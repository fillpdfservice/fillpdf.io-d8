<?php

namespace Drupal\fps_api\Event;

/**
 * Contains event names for FillPDF Service API.
 */
final class FpsApiEvents {

  /**
   * Before API calls.
   */
  public const PRE_HANDLE = 'fps_api.pre_handle';

  /**
   * After a merge API call.
   */
  public const POST_HANDLE = 'fps_api.post_handle';

}
