<?php

namespace Drupal\fps_api\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event context for post-merge event.
 */
class PreHandleEvent extends Event {

}
