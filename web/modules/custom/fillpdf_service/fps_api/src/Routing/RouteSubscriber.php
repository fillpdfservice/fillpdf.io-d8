<?php

namespace Drupal\fps_api\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Duplicate /xmlrpc to /xmlrpc.php.
    $xmlrpc_alias = clone $collection->get('xmlrpc');
    if ($xmlrpc_alias !== NULL) {
      $xmlrpc_alias->setPath('/xmlrpc.php');
    }

    $collection->add('fps_api.xmlrpc', $xmlrpc_alias);
  }

}
