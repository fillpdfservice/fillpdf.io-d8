<?php

namespace Drupal\fps_api;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use finfo;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Static methods to help validate API parameters.
 */
class ApiParameterValidator {

  /**
   * Ensures the PDF variable is provided.
   *
   * @param string $pdf
   *   The PDF to check.
   */
  public static function ensurePdfNotEmpty($pdf): void {
    if (empty($pdf)) {
      throw new BadRequestHttpException(self::getFailureMessage());
    }
  }

  /**
   * Get a failure message.
   *
   * This is to avoid variable translation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The failure message.
   */
  private static function getFailureMessage(): TranslatableMarkup {
    return t("You must include a Base64-encoded PDF in the 'pdf' key of your request.");
  }

  /**
   * Ensures the PDF file is actually a PDF.
   *
   * @param string $pdf
   *   The PDF to check.
   */
  public static function ensureFileIsPdf($pdf): void {
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mime_type = $finfo->buffer($pdf);
    if ($pdf === FALSE || $mime_type !== 'application/pdf') {
      throw new BadRequestHttpException(self::getFailureMessage());
    }
  }

}
