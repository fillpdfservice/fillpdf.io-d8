<?php

namespace Drupal\fps_api\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\fillpdf\FieldMapping\ImageFieldMapping;
use Drupal\fillpdf\FieldMapping\TextFieldMapping;
use Drupal\fillpdf_legacy\Plugin\BackendServiceManager;
use Drupal\fps_api\ApiParameterValidator;
use Drupal\fps_api\Event\FpsApiEvents;
use Drupal\fps_api\Event\PostHandleEvent;
use Drupal\fps_api\Event\PreHandleEvent;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "fps_merge_v1",
 *   label = @Translation("FillPDF API: Merge"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/merge"
 *   }
 * )
 */
class MergeV1 extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The request stack, from which we get the current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $currentRequest;

  /**
   * The backend service manager.
   *
   * @var \Drupal\fillpdf_legacy\Plugin\BackendServiceManager
   */
  protected $backendServiceManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new MergeV1 object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\fillpdf_legacy\Plugin\BackendServiceManager $backend_service_manager
   *   The backend service manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    RequestStack $request_stack,
    ConfigFactoryInterface $config,
    BackendServiceManager $backend_service_manager,
    EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->backendServiceManager = $backend_service_manager;
    $this->config = $config;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('fps_api'),
      $container->get('current_user'),
      $container->get('request_stack'),
      // Can't control what $configuration gets injected, so we have to pull it
      // ourselves.
      $container->get('config.factory'),
      $container->get('plugin.manager.fillpdf_backend_service'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Receives a PDF and a list of fields and merges them together.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function post(): ResourceResponse {
    $data = \GuzzleHttp\json_decode($this->currentRequest->getContent(), TRUE);

    $this->eventDispatcher->dispatch(FpsApiEvents::PRE_HANDLE, new PreHandleEvent());

    if (empty($data['fields']) || !is_array($data['fields'])) {
      throw new BadRequestHttpException($this->t("The 'fields' argument must contain an object mapping PDF field keys to data values."));
    }

    $pdf = !empty($data['pdf']) ? base64_decode($data['pdf']) : NULL;

    ApiParameterValidator::ensurePdfNotEmpty($pdf);
    ApiParameterValidator::ensureFileIsPdf($pdf);

    $request_fields = $data['fields'];

    // Remove non-option keys from $data.
    $known_options = ['flatten'];
    $options = array_intersect_key($data, array_flip($known_options));

    // Ensure default values.
    $options += ['flatten' => TRUE];

    $fillpdf_config = $this->config->get('fillpdf.settings');

    $backend = $fillpdf_config->get('backend');
    /** @var \Drupal\fillpdf\Plugin\BackendServiceInterface $fillpdf */
    $fillpdf = $this->backendServiceManager->createInstance($backend, $fillpdf_config->get());

    // Convert JSON fields into the objects expected by the plugin. Throw an
    // error if we don't understand what they mean in order to help users avoid
    // getting frustrated by silent failures.
    $fields = [];
    foreach ($request_fields as $field_name => $request_field) {
      if (is_string($request_field)) {
        $fields[$field_name] = new TextFieldMapping($request_field);
        continue;
      }

      if (is_array($request_field)) {
        if (!isset($request_field['data'])) {
          throw new BadRequestHttpException("Missing sub-key 'data' in mapping for {$field_name}.");
        }
        if (empty($request_field['type'])) {
          $request_field['type'] = 'text';
        }
        switch ($request_field['type']) {
          case 'text':
            $fields[$field_name] = new TextFieldMapping($request_field['data']);
            continue 2;

          case 'image':
            // We used to require the 'extension' parameter, but we don't
            // anymore. It is still part of ImageFieldMapping because other
            // BackendService plugins (e.g. pdftk and similar?) may want to save
            // a temporary image file for their purposes and may have use for
            // it. Anyway, set it to NULL if they have not provided it.
            $request_field += ['extension' => NULL];
            try {
              $fields[$field_name] = new ImageFieldMapping(base64_decode($request_field['data']), $request_field['extension']);
            }
            catch (InvalidArgumentException $iae) {
              // We know what went wrong here because this is the only exception
              // that can be thrown in this situation.
              throw new BadRequestHttpException("Invalid image extension in mapping for {$field_name}. Valid extensions: jpg, png, gif");
            }
            continue 2;

          default:
            throw new BadRequestHttpException("Invalid mapping type for {$field_name}. Valid types: text, image");
        }
      }
    }

    $merged_pdf = $fillpdf->merge($pdf, $fields, $options);

    $this->eventDispatcher->dispatch(FpsApiEvents::POST_HANDLE, new PostHandleEvent());

    return new ResourceResponse(['pdf' => base64_encode($merged_pdf)]);
  }

}
