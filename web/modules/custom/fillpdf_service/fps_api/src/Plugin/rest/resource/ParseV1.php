<?php

namespace Drupal\fps_api\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\fillpdf_legacy\Plugin\BackendServiceManager;
use Drupal\fps_api\ApiParameterValidator;
use Drupal\fps_api\Event\FpsApiEvents;
use Drupal\fps_api\Event\PostHandleEvent;
use Drupal\fps_api\Event\PreHandleEvent;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "fps_parse_v1",
 *   label = @Translation("FillPDF API: Parse"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/parse"
 *   }
 * )
 */
class ParseV1 extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The request stack, from which we get the current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $currentRequest;

  /**
   * The backend service manager.
   *
   * @var \Drupal\fillpdf_legacy\Plugin\BackendServiceManager
   */
  protected $backendServiceManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new ParseV1 object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\fillpdf_legacy\Plugin\BackendServiceManager $backend_service_manager
   *   The backend service manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    RequestStack $request_stack,
    ConfigFactoryInterface $config,
    BackendServiceManager $backend_service_manager,
    EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->backendServiceManager = $backend_service_manager;
    $this->config = $config;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('fps_api'),
      $container->get('current_user'),
      $container->get('request_stack'),
      // Can't control what config gets injected, so we have to pull it
      // ourselves.
      $container->get('config.factory'),
      $container->get('plugin.manager.fillpdf_backend_service'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of fields for a PDF.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post(): ResourceResponse {
    $data = \GuzzleHttp\json_decode($this->currentRequest->getContent(), TRUE);

    $this->eventDispatcher->dispatch(FpsApiEvents::PRE_HANDLE, new PreHandleEvent());

    $pdf = !empty($data['pdf']) ? base64_decode($data['pdf']) : NULL;

    ApiParameterValidator::ensurePdfNotEmpty($pdf);
    ApiParameterValidator::ensureFileIsPdf($pdf);

    /** @var \Drupal\fillpdf\Plugin\BackendServiceInterface $fillpdf */
    $fillpdf_settings = $this->config->get('fillpdf.settings');
    $fillpdf = $this->backendServiceManager->createInstance(
      $fillpdf_settings
        ->get('backend'),
      // Also pass the settings in general so that the service can find the
      // local server.
      $fillpdf_settings->get()
    );

    $response = new ResourceResponse([
      'fields' => $fillpdf->parse($pdf),
    ]);

    $this->eventDispatcher->dispatch(FpsApiEvents::POST_HANDLE, new PostHandleEvent());

    return $response;
  }

}
