<?php

namespace Drupal\fps_api\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class ApiKeyV1.
 */
class ApiKeyV1 implements AuthenticationProviderInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\StringTranslation\Translator\TranslatorInterface $translator
   *   The translator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, TranslatorInterface $translator) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $translator;
  }

  /**
   * Checks whether suitable authentication credentials are on the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return bool
   *   TRUE if authentication credentials suitable for this provider are on the
   *   request, FALSE otherwise.
   */
  public function applies(Request $request) {
    $content = $request->getContent();
    $content_type = $request->getContentType();
    if ($content_type === 'json' && $content) {
      $data = \GuzzleHttp\json_decode($content, TRUE);
      if (count($data) && !empty($data['api_key'])) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function authenticate(Request $request) {
    $data = \GuzzleHttp\json_decode($request->getContent(), TRUE);
    $user_query = $this->entityTypeManager->getStorage('user')->getQuery();
    $matching_user = NULL;
    /** @var array $matching_uids */
    $matching_uids = $user_query
      ->condition('fps_api_key', $data['api_key'])
      ->execute();

    if (count($matching_uids)) {
      $matching_user = User::load(reset($matching_uids));
    }

    if ($matching_user) {
      // Return the user.
      return $matching_user;
    }

    (new JsonResponse(
      [
        'message' => $this->t('The FillPDF Service API key you provided is incorrect.'),
      ],
      403))
      ->send();
  }

  /**
   * {@inheritdoc}
   */
  public function cleanup(Request $request) {
  }

  /**
   * {@inheritdoc}
   */
  public function handleException(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();
    if ($exception instanceof AccessDeniedHttpException) {
      $event->setException(
        new UnauthorizedHttpException('Invalid consumer origin.', $exception)
      );
      return TRUE;
    }
    return FALSE;
  }

}
