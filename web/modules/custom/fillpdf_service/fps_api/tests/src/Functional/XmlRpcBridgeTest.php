<?php

namespace Drupal\Tests\fps_api\Functional;

use Drupal\fillpdf\Entity\FillPdfForm;
use Drupal\fillpdf\FieldMapping\ImageFieldMapping;
use Drupal\fillpdf\FieldMapping\TextFieldMapping;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\fillpdf\Traits\TestFillPdfTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * @group fps_api
 */
class XmlRpcBridgeTest extends BrowserTestBase {

  use TestFillPdfTrait;
  use TestFileCreationTrait;

  public static $modules = ['fps_api_test', 'fillpdf_test'];

  /**
   * Test the XML-RPC BC layer.
   *
   * Configure the FillPDF module to point at this site, and manually use the
   * FillPDF Service plugin. We have to use it manually because we're using
   * the test plugin for the site itself, and this test just needs to prove
   * that we are calling the API properly for stuff. It should basically
   * mirror the API test, but via the FillPDF Service plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @group legacy
   */
  public function testXmlRpcBridge(): void {
    $admin_user = $this->drupalCreateUser([], 'fps_api', TRUE);
    $this->drupalLogin($admin_user);

    $normal_user = $this->drupalCreateUser(['administer site configuration']);

    // Set the API key.
    $normal_user->addRole('fillpdf_service');
    // NOTE: We don't log in the user because the API does that via API key,
    // and we want this test to resemble actual use (which is anonymous).
    $normal_user->save();
    $api_key = $normal_user->fps_api_key->value;

    // Configure test FillPDF plugin.
    $this->config('fillpdf.settings')
      ->set('backend', 'test')
      ->set('template_scheme', 'public')
      ->save();

    $backend_manager = $this->container->get('plugin.manager.fillpdf_backend');
    $request = $this->container->get('request_stack')
      ->getCurrentRequest();
    $configuration = [
      'remote_protocol' => $request->getScheme(),
      'remote_endpoint' => $request->getHttpHost() . '/xmlrpc.php',
      'fillpdf_service_api_key' => $api_key,
    ];
    $backend = $backend_manager->createInstance('fps_api_test_fps_v3', $configuration);

    $test_pdf = file_get_contents(drupal_get_path('module', 'fillpdf') . '/tests/modules/fillpdf_test/files/fillpdf_test_v3.pdf');

    // This will use the test backend, but that's fine. We mostly just need the
    // record to pass to ->parse().
    $this->uploadTestPdf('fillpdf_test_v3.pdf');
    $fillpdf_form = FillPdfForm::load($this->getLatestFillPdfForm());

    /*
     * PARSING.
     */
    $pdf_data = base64_encode($test_pdf);

    $fields = $backend->parse($fillpdf_form);

    // Since we use the test plugin for tests, we expect the response to be the
    // same whether executed directly or received from the API.
    static::assertEmpty($fields, 'Parse requests are rejected, and the user is told to upgrade.');

    /*
     * MERGING.
     */
    // Standard request works.
    $image_files = $this->getTestFiles('image');
    foreach ($image_files as $image_file) {
      $extension = pathinfo($image_file->filename, PATHINFO_EXTENSION);
      if ($extension === 'jpg') {
        $image = $image_file;
        break;
      }
    }
    $image_path_info = pathinfo($image->uri);
    $realpath = $this->container->get('file_system')->realpath($image->uri);
    $image_contents = file_get_contents($image->uri);
    $base64_image = base64_encode($image_contents);
    $field_mapping = [
      'fields' => [
        'TextField' => 'bar',
        'Button' => '{image}' . $realpath,
        'ImageField' => '{image}' . $realpath,
      ],
      'images' => [
        'Button' => [
          'data' => $base64_image,
          'filenamehash' => md5($image_path_info['filename']) . '.' . $image_path_info['extension'],
        ],
        'ImageField' => [
          'data' => $base64_image,
          'filenamehash' => md5($image_path_info['filename']) . '.' . $image_path_info['extension'],
        ],
      ],
    ];
    $context = [
      'flatten' => FALSE,
    ];

    $result = $backend->populateWithFieldData($fillpdf_form, $field_mapping, $context);
    static::assertEquals($test_pdf, $result);
    $merge_state = $this->container->get('state')
      ->get('fillpdf_test.last_populated_metadata');
    $mapping_state = $merge_state['field_mapping'];
    static::assertInstanceOf(TextFieldMapping::class, $mapping_state['TextField']);
    /** @var \Drupal\fillpdf\FieldMapping\ImageFieldMapping $button */
    $button = $mapping_state['Button'];
    /** @var \Drupal\fillpdf\FieldMapping\ImageFieldMapping $image_field */
    $image_field = $mapping_state['ImageField'];
    static::assertInstanceOf(ImageFieldMapping::class, $button);
    static::assertInstanceOf(ImageFieldMapping::class, $image_field);
    static::assertEquals('jpg', $button->getExtension());
    static::assertEquals($image_contents, $button->getData());
    static::assertEquals('jpg', $image_field->getExtension());
    static::assertEquals($image_contents, $image_field->getData());
    static::assertFalse($merge_state['options']['flatten']);

    // Ensure state doesn't get polluted for the next test.
    $this->container->get('state')
      ->delete('fillpdf_test.last_populated_metadata');

    // XML-RPC requests to merge_pdf, merge_pdf_enhanced -- just ensure they
    // don't error out...and run the same assertions, I guess..
    $v1_backend = $backend_manager->createInstance('fps_api_test_fps_v1', $configuration);
    $bad_result = @$v1_backend->populateWithFieldData($fillpdf_form, $field_mapping, $context);
    static::assertEmpty($bad_result, 'Missing image data causes an API error.');

    // Ensure state doesn't get polluted for the next test.
    $this->container->get('state')
      ->delete('fillpdf_test.last_populated_metadata');

    $legacy_mapping = $field_mapping;
    unset($legacy_mapping['fields']['Button']);
    unset($legacy_mapping['fields']['ImageField']);

    $result = $v1_backend->populateWithFieldData($fillpdf_form, $legacy_mapping, $context);
    static::assertEquals($test_pdf, $result, 'The old merge_pdf endpoint still works.');
    $merge_state = $this->container->get('state')
      ->get('fillpdf_test.last_populated_metadata');
    $mapping_state = $merge_state['field_mapping'];
    static::assertInstanceOf(TextFieldMapping::class, $mapping_state['TextField']);
    // Flattening wasn't supported in merge_pdf, so it will always be the
    // default value of TRUE.
    static::assertTrue($merge_state['options']['flatten']);

    // Ensure state doesn't get polluted for the next test.
    $this->container->get('state')
      ->delete('fillpdf_test.last_populated_metadata');

    // XML-RPC requests to merge_pdf, merge_pdf_enhanced -- just ensure they
    // don't error out...and run the same assertions, I guess..
    $v2_backend = $backend_manager->createInstance('fps_api_test_fps_v2', $configuration);
    $result = $v2_backend->populateWithFieldData($fillpdf_form, $legacy_mapping, $context);
    static::assertEquals($test_pdf, $result, 'The old merge_pdf_enhanced endpoint still works.');
    $merge_state = $this->container->get('state')
      ->get('fillpdf_test.last_populated_metadata');
    $mapping_state = $merge_state['field_mapping'];
    static::assertInstanceOf(TextFieldMapping::class, $mapping_state['TextField']);
    // It did handle flattening, though.
    static::assertFalse($merge_state['options']['flatten']);

    // Empty API key fails.
    $empty_api_key_configuration = $configuration;
    $empty_api_key_configuration['fillpdf_service_api_key'] = '';
    $empty_api_key_backend = $backend_manager->createInstance('fps_api_test_fps_v3', $empty_api_key_configuration);
    // Use the suppression operator because $result->data will be empty and the
    // current FillPDF Service backend doesn't handle that well.
    $result = @$empty_api_key_backend->populateWithFieldData($fillpdf_form, $field_mapping, $context);
    static::assertEmpty($result, 'Empty API key fails over XML-RPC too.');

    // Bad API key fails.
    $bad_api_key_configuration = $configuration;
    $bad_api_key_configuration['fillpdf_service_api_key'] = 'BAD';
    $bad_api_key_backend = $backend_manager->createInstance('fps_api_test_fps_v3', $bad_api_key_configuration);
    $result = @$bad_api_key_backend->populateWithFieldData($fillpdf_form, $field_mapping, $context);
    static::assertEmpty($result, 'Bad API key fails over XML-RPC too.');
  }

}
