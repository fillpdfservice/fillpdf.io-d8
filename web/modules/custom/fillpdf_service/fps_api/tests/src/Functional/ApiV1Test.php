<?php

namespace Drupal\Tests\fps_api\Functional;

use Drupal\Core\Url;
use Drupal\fillpdf\FieldMapping\ImageFieldMapping;
use Drupal\fillpdf\FieldMapping\TextFieldMapping;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\XdebugRequestTrait;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\RequestOptions;

/**
 * Test FillPDF Service API endpoints.
 *
 * @group fps_api
 *
 * @todo: make separate test classes for each endpoint, derived from \Drupal\Tests\rest\Functional\ResourceTestBase
 */
class ApiV1Test extends BrowserTestBase {
  use TestFileCreationTrait;
  use XdebugRequestTrait;

  const FPS_API_TEST_KEY = 'fps_api_test';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['fillpdf_test', 'fps_api'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;
  protected $requestOptions;

  /**
   * @var \Drupal\fillpdf_test\Plugin\FillPdfBackend\TestFillPdfBackend
   */
  protected $testFillPdfBackend;

  /**
   * @var \Drupal\fillpdf_test\Plugin\BackendService\Test
   */
  protected $testBackendService;

  /**
   * Tests /api/v1/parse.
   */
  public function testParse() {
    $endpoint = $this->buildUrl(Url::fromRoute('rest.fps_parse_v1.POST'));
    // @todo: Make separate fps_api_test module
    $test_pdf = file_get_contents(drupal_get_path('module', 'fillpdf') . '/tests/modules/fillpdf_test/files/fillpdf_test_v3.pdf');

    $api_key = $this->user->fps_api_key->value;
    $request_args = [
      'api_key' => $api_key,
      'pdf' => base64_encode($test_pdf),
    ];
    $response = $this->makeRequest($endpoint, $request_args);
    static::assertEquals(200, $response->getStatusCode(), 'A request with a valid API key and valid PDF works.');
    $response_body = $response->getBody();
    static::assertNotEmpty($response_body, 'The API returned something in the body.');
    $response_data = \GuzzleHttp\json_decode($response_body, TRUE);
    // Since we use the test plugin for tests, we expect the response to be the
    // same whether executed directly or received from the API.
    static::assertEquals($this->testBackendService->parse($test_pdf), $response_data['fields']);

    // @todo: split out stuff that's really just testing the Authentication Provider from that which is testing the actual Parse v1 method.
    $invalid_key_request_args = $request_args;
    $invalid_key_request_args['api_key'] = 'invalid api key';
    $response = $this->makeRequest($endpoint, $invalid_key_request_args);
    static::assertEquals(403, $response->getStatusCode(), 'A request with an invalid API key is rejected.');

    $empty_key_request_args = $request_args;
    unset($empty_key_request_args['api_key']);
    $response = $this->makeRequest($endpoint, $empty_key_request_args);
    static::assertEquals(403, $response->getStatusCode(), 'A request with an empty API key is rejected.');

    $bad_pdf_request_args = $request_args;
    $bad_pdf_request_args['pdf'] = 'This is just some random string. It is not even a PDF.';
    $response = $this->makeRequest($endpoint, $bad_pdf_request_args);
    static::assertEquals(400, $response->getStatusCode(), 'A request with a string instead of a Base64-encoded PDF file is rejected.');
  }

  /**
   * @param $endpoint
   * @param $request_args
   * @return \Psr\Http\Message\ResponseInterface
   */
  protected function makeRequest($endpoint, $request_args) {
    $payload = \GuzzleHttp\json_encode($request_args);
    $options = array_merge($this->requestOptions, [
      'headers' => [
        'Content-Type' => 'application/json',
      ],
      'body' => $payload,
    ]);

    // Forward relevant Xdebug cookies.
    // Inspired by \Drupal\rest\Tests\RESTTestBase::cookies()
    $cookie_jar = new CookieJar();
    $request = $this->container->get('request_stack')->getCurrentRequest();
    $cookies = $this->extractCookiesFromRequest($request);
    if (!empty($cookies)) {
      foreach ($cookies as $key => $cookie_values) {
        foreach ($cookie_values as $cookie_value) {
          // setcookie() sets the value of a cookie to be deleted, when its gonna
          // be removed.
          if ($cookie_value !== 'deleted') {
            $cookie_jar->setCookie(new SetCookie([
              'Name' => $key,
              'Value' => $cookie_value,
              'Domain' => $request->getHost(),
            ]));
          }
        }
      }

      $options['cookies'] = $cookie_jar;
    }

    return $this->httpClient->post($endpoint, $options);
  }

  /**
   * Tests /api/v1/merge.
   */
  public function testMerge() {
    $endpoint = $this->buildUrl(Url::fromRoute('rest.fps_merge_v1.POST'));
    $test_pdf = file_get_contents(drupal_get_path('module', 'fillpdf') . '/tests/modules/fillpdf_test/files/fillpdf_test_v3.pdf');
    $api_key = $this->user->fps_api_key->value;
    $request_args = [
      'api_key' => $api_key,
      'pdf' => base64_encode($test_pdf),
      'fields' => [
        'Foo' => 'bar',
      ],
    ];
    $response = $this->makeRequest($endpoint, $request_args);
    $response_data = \GuzzleHttp\json_decode((string) $response->getBody(), TRUE);
    static::assertEquals(200, $response->getStatusCode(), 'API request was successful.');
    static::assertEquals($test_pdf, base64_decode($response_data['pdf']), 'Result PDF is as expected.');
    static::assertEquals($this->testBackendService->merge($test_pdf, [], []), base64_decode($response_data['pdf']), 'Result PDF is the same as via the backend plugin.');

    $bad_pdf_request_args = $request_args;
    $bad_pdf_request_args['pdf'] = 'This is just some random string. It is not even a PDF.';
    $response = $this->makeRequest($endpoint, $bad_pdf_request_args);
    static::assertEquals(400, $response->getStatusCode(), 'A request with a string instead of a Base64-encoded PDF file is rejected.');

    $missing_data_key_request_args = $request_args;
    $missing_data_key_request_args['fields']['Foo2'] = [
      'type' => 'text',
    ];
    $response = $this->makeRequest($endpoint, $missing_data_key_request_args);
    static::assertEquals(400, $response->getStatusCode(), "A request with a field missing the 'data' key is rejected.");

    $missing_extension_key_request_args = $request_args;
    $missing_extension_key_request_args['fields']['Foo2'] = [
      'type' => 'image',
      'data' => 'dummy image',
    ];
    $response = $this->makeRequest($endpoint, $missing_extension_key_request_args);
    static::assertEquals(200, $response->getStatusCode(), "A request with an image field missing the 'extension' key works.");

    $invalid_image_ext_request = $request_args;
    $invalid_image_ext_request['fields']['Foo2'] = [
      'type' => 'image',
      'data' => 'dummy image',
      'extension' => 'bmp',
    ];
    $response = $this->makeRequest($endpoint, $invalid_image_ext_request);
    static::assertEquals(400, $response->getStatusCode(), "A request with an image field containing an invalid extension is rejected.");

    $invalid_type_request = $request_args;
    $invalid_type_request['fields']['Foo2'] = [
      'type' => 'FAILURE!',
      'data' => 'this does not even matter',
    ];
    $response = $this->makeRequest($endpoint, $invalid_type_request);
    static::assertEquals(400, $response->getStatusCode(), "A request with a field containing an invalid type is rejected.");

    // Ensure state doesn't get polluted for the next test.
    $this->container->get('state')
      ->delete('fillpdf_test.last_populated_metadata');

    $flatten_request_args = $request_args;
    $flatten_request_args['flatten'] = TRUE;
    $response = $this->makeRequest($endpoint, $flatten_request_args);
    $merge_state = $this->container->get('state')
      ->get('fillpdf_test.last_populated_metadata');
    self::assertTrue($merge_state['options']['flatten']);

    // Ensure state doesn't get polluted for the next test.
    $this->container->get('state')
      ->delete('fillpdf_test.last_populated_metadata');

    $image_files = $this->getTestFiles('image');
    foreach ($image_files as $image_file) {
      $extension = pathinfo($image_file->filename, PATHINFO_EXTENSION);
      if ($extension === 'jpg') {
        $image = $image_file;
        break;
      }
    }
    $field_request_args = $request_args;
    $field_request_args['fields'] = [
      'Foo' => 'bar',
      'Foo2' => [
        'type' => 'text',
        'data' => 'bar2',
      ],
      'Image1' => [
        'type' => 'image',
        'data' => base64_encode(file_get_contents($image->uri)),
        'extension' => 'jpg',
      ],
    ];
    $this->makeRequest($endpoint, $field_request_args);
    $merge_state = $this->container->get('state')
      ->get('fillpdf_test.last_populated_metadata');

    // Check that fields are set as expected.
    self::assertInstanceOf(TextFieldMapping::class, $merge_state['field_mapping']['Foo'], 'Field "Foo" was mapped to a TextFieldMapping object.');
    self::assertInstanceOf(TextFieldMapping::class, $merge_state['field_mapping']['Foo2'], 'Field "Foo2" was mapped to a TextFieldMapping object.');
    self::assertInstanceOf(ImageFieldMapping::class, $merge_state['field_mapping']['Image1'], 'Field "Image1" was mapped to an ImageFieldMapping object.');
    self::assertStringEqualsFile($image->uri, $merge_state['field_mapping']['Image1']->getData(), 'Contents of mapped image same as contents of image sent to API.');
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration']);

    // Set the API key.
    $this->user->addRole('fillpdf_service');
    // NOTE: We don't log in the user because the API does that via API key,
    // and we want this test to resemble actual use (which is anonymous).
    $this->user->save();

    $this->httpClient = $this->container->get('http_client');
    $this->requestOptions = [
      RequestOptions::HTTP_ERRORS => FALSE,
    ];

    $this->testFillPdfBackend = $this->container->get('plugin.manager.fillpdf_backend')
      ->createInstance('test', []);

    $this->testBackendService = $this->container->get('plugin.manager.fillpdf_backend_service')
      ->createInstance('test');

    // Configure test FillPDF plugin.
    $this->config('fillpdf.settings')
      ->set('backend', 'test')
      ->save();

    $this->rebuildAll();
  }

}
