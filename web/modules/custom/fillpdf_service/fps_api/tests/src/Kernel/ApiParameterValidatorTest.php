<?php

namespace Drupal\Tests\fps_api\Kernel;

use Drupal\fps_api\ApiParameterValidator;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\fps_api\Unit\Fixtures\PdfFixture;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @covers \Drupal\fps_api\ApiParameterValidator
 */
class ApiParameterValidatorTest extends KernelTestBase {

  public function testPdfValidation() {
    $pdf = '';
    $fail = FALSE;
    try {
      ApiParameterValidator::ensurePdfNotEmpty($pdf);
    }
    catch (BadRequestHttpException $bre) {
      $fail = TRUE;
    }

    self::assertTrue($fail, 'Empty PDF causes exception to be thrown.');

    $pdf = 'This is just some random string and not a PDF.';
    $fail = FALSE;
    try {
      ApiParameterValidator::ensureFileIsPdf($pdf);
    }
    catch (BadRequestHttpException $bre2) {
      $fail = TRUE;
    }

    self::assertTrue($fail, 'Non-PDF causes exception to be thrown.');

    $pdf = PdfFixture::getTestPdf();
    $fail = FALSE;

    ApiParameterValidator::ensurePdfNotEmpty($pdf);
    self::assertFalse($fail, 'Non-empty PDF passes validation.');

    ApiParameterValidator::ensureFileIsPdf($pdf);
    self::assertFalse($fail, 'File that is a PDF passes validation.');
  }

}
