<?php

namespace Drupal\fps_commerce\Plugin\Commerce\SubscriptionType;

use Drupal\commerce_license\Plugin\Commerce\SubscriptionType\LicenseSubscription;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_recurring\BillingPeriod;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\commerce_recurring_metered\SubscriptionFreeUsageInterface;
use Drupal\commerce_recurring_metered\UsageProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A subscription to the FillPDF Service API.
 *
 * @CommerceSubscriptionType(
 *  id = "fps_commerce_api_license",
 *  label = @Translation("FillPDF Service API License"),
 * )
 */
class ApiLicense extends LicenseSubscription implements SubscriptionFreeUsageInterface {

  /**
   * The usage proxy.
   *
   * @var \Drupal\commerce_recurring_metered\UsageProxyInterface
   */
  protected $usageProxy;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, UsageProxyInterface $usage_proxy) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->usageProxy = $usage_proxy;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_recurring_metered.usage_proxy'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFreeQuantity(ProductVariationInterface $variation, SubscriptionInterface $subscription) {
    $purchased_entity_id = $subscription->getPurchasedEntityId();
    if (!$purchased_entity_id) {
      throw new InvalidArgumentException('The passed-in subscription does not contain a purchased entity ID. Unable to determine free quantity.');
    }
    /** @var \Drupal\commerce_product\Entity\ProductVariation $purchased_entity */
    $purchased_entity = $this->entityTypeManager->getStorage('commerce_product_variation')->load($purchased_entity_id);
    if (!$purchased_entity) {
      throw new InvalidArgumentException('The passed-in subscription does not contain a valid purchased entity. Unable to determine free quantity.');
    }
    return $purchased_entity->get('field_service_credits')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function collectCharges(SubscriptionInterface $subscription, BillingPeriod $billing_period) {
    $charges = parent::collectCharges($subscription, $billing_period);
    $usage_charges = $this->usageProxy->collectCharges($subscription, $billing_period);

    return array_merge($charges, $usage_charges);
  }

}
