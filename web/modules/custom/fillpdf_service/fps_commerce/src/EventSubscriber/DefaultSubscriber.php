<?php

namespace Drupal\fps_commerce\EventSubscriber;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_recurring_metered\UsageProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\fps_api\Event\FpsApiEvents;
use Drupal\fps_commerce\Subscriptions\SubscriptionCheckerInterface;
use LogicException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class DefaultSubscriber.
 */
class DefaultSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The authenticated user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The usage proxy.
   *
   * @var \Drupal\commerce_recurring_metered\UsageProxyInterface
   */
  protected $usageProxy;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The subscription checker.
   *
   * @var \Drupal\fps_commerce\Subscriptions\SubscriptionCheckerInterface
   */
  protected $subscriptionChecker;

  /**
   * Constructs a new DefaultSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The authenticated user.
   * @param \Drupal\commerce_recurring_metered\UsageProxyInterface $usage_proxy
   *   The usage proxy.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\fps_commerce\Subscriptions\SubscriptionCheckerInterface $subscriptionChecker
   *   The subscription checker service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, UsageProxyInterface $usage_proxy, RequestStack $request_stack, SubscriptionCheckerInterface $subscriptionChecker) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->usageProxy = $usage_proxy;
    $this->requestStack = $request_stack;
    $this->subscriptionChecker = $subscriptionChecker;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[FpsApiEvents::PRE_HANDLE] = ['preHandle'];
    $events[FpsApiEvents::POST_HANDLE] = ['postHandle'];

    return $events;
  }

  /**
   * Handles fps_api.pre_merge.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function preHandle(): void {
    $user_subscription = $this->subscriptionChecker->getUserSubscription($this->currentUser);
    if (!$user_subscription) {
      // @codingStandardsIgnoreStart
      /** @noinspection NullPointerExceptionInspection */
      // @codingStandardsIgnoreEnd
      $pricing_link = Url::fromUri($this->requestStack->getCurrentRequest()
        ->getSchemeAndHttpHost() . '/pricing', ['absolute' => TRUE])
        ->toString();
      throw new AccessDeniedHttpException("Your API key is valid, but you have not yet subscribed. Please go to $pricing_link and subscribe to a plan.");
    }

    // If the subscription is valid and the user allows overages, we do not need
    // to check how many service credits they have remaining.
    $overages_allowed = $this->currentUser->getAccount()->field_allow_overages->value;
    if ($overages_allowed) {
      return;
    }

    $subscription_product = $this->subscriptionChecker->getSubscribedProduct($user_subscription);
    if (!$subscription_product) {
      throw new LogicException('Subscription does not contained purchased entity. This is a bug. Cannot proceed.');
    }
    $service_credits = $subscription_product->get('field_service_credits')->value;

    $order = $user_subscription->getCurrentOrder();
    if (!$order) {
      throw new LogicException('Subscription missing an underlying order.');
    }

    /** @var \Drupal\commerce_recurring\Plugin\Field\FieldType\BillingPeriodItem $field_billing_period */
    $field_billing_period = $order->get('billing_period')->first();
    $usage = $this->usageProxy->getUsageForPeriod($user_subscription, $field_billing_period->toBillingPeriod());
    $usage_variation = $this->getMeteredUsageVariation($subscription_product);
    if (!$usage_variation) {
      throw new LogicException("Unable to load variation for tracking metered usage. Is one set for the product in the subscription? (ID: {$subscription_product->id()})");
    }
    if (empty($usage['counter'][$usage_variation->id()])) {
      $current_usage = 0;
    }
    else {
      $record = reset($usage['counter'][$usage_variation->id()]);
      $current_usage = $record->getQuantity();
    }

    if ($current_usage >= $service_credits) {
      $user_profile_link = Url::fromRoute('entity.user.edit_form', ['user' => $this->currentUser->id()])
        ->toString();
      throw new HttpException(402, "This subscription has no remaining service credits. You must either wait until the next billing period or enable the \"Let me go over the usage limit and pay per service credit\" option in your user profile at $user_profile_link.");
    }
  }

  /**
   * Handles fps_api.post_handle.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function postHandle(): void {
    $user_subscription = $this->subscriptionChecker->getUserSubscription($this->currentUser);
    if (!$user_subscription) {
      throw new LogicException('Missing subscription.');
    }

    $subscription_product = $this->subscriptionChecker->getSubscribedProduct($user_subscription);
    if (!$subscription_product) {
      throw new LogicException('Subscription does not contained purchased entity. This is a bug. Cannot proceed.');
    }

    $usage_variation = $this->getMeteredUsageVariation($subscription_product);
    if (!$usage_variation) {
      throw new LogicException("Unable to load variation for tracking metered usage. Is one set for the product in the subscription? (ID: {$subscription_product->id()})");
    }

    $this->usageProxy->addUsage($user_subscription, $usage_variation);
  }

  /**
   * Gets the product variation used for tracking metered usage.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $subscription_product
   *   The subscription product.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariation|null
   *   The product variation used for tracking metered usage.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getMeteredUsageVariation(ProductVariation $subscription_product): ?ProductVariation {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $field_metered_usage_product */
    $field_metered_usage_product = $subscription_product->get('field_metered_usage_product');
    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $first_target */
    $first_target = $usage_variation = $field_metered_usage_product->first();
    /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $entity_reference */
    $entity_reference = $first_target->get('entity');
    $target_entity = $entity_reference->getTarget();
    if ($target_entity === NULL) {
      throw new LogicException('Metered usage product is missing.');
    }
    /** @var \Drupal\commerce_product\Entity\ProductVariation $usage_variation */
    $usage_variation = $target_entity->getValue();

    return $usage_variation;
  }

}
