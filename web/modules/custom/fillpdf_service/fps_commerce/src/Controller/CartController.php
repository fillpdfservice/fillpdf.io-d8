<?php

namespace Drupal\fps_commerce\Controller;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * A class to take over the cart page and prevent the user from landing on it.
 */
class CartController extends ControllerBase {

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   *
   */
  public function __construct(CartProviderInterface $cartProvider) {
    $this->cartProvider = $cartProvider;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('commerce_cart.cart_provider'));
  }

  /**
   * Skips the cart by redirecting the user to checkout or the homepage.
   */
  public function skipCart(): RedirectResponse {
    $cartIds = $this->cartProvider->getCartIds();
    if (!$cartIds) {
      return $this->redirectToHomepage();
    }
    $order_id = max($cartIds);

    $etm = $this->entityTypeManager();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $etm->getStorage('commerce_order')->load($order_id);
    // Start checkout as long as the order has items.
    if ($order->hasItems()) {
      $checkout_url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])->toString();
      return new RedirectResponse($checkout_url);
    }

    return $this->redirectToHomepage();
  }

  /**
   * Return a redirect to the homepage.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The RedirectResponse.
   */
  protected function redirectToHomepage(): RedirectResponse {
    return new RedirectResponse(Url::fromRoute('<front>'));
  }

}
