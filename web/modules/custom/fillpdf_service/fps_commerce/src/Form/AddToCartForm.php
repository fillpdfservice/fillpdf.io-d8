<?php

namespace Drupal\fps_commerce\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;

/**
 * Various helper methods for add-to-cart forms.
 */
class AddToCartForm {

  /**
   * Clear the cart, except for the item we just added.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function trimCart(array &$form, FormStateInterface $formState): void {
    $entityTypeManager = Drupal::entityTypeManager();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
    $cart = $entityTypeManager->getStorage('commerce_order')
      ->load($formState->get('cart_id'));
    $items = $cart->getItems();
    // Meh, what can ya do?
    if (empty($items)) {
      return;
    }
    $how_many = count($items);
    $currentOrderItem = 1;
    $item = NULL;
    foreach ($items as $item) {
      if ($currentOrderItem < $how_many) {
        $cart->removeItem($item);
      }
    }

    // Make sure there's only one of the last item.
    $item->setQuantity(1);
  }

  /**
   * Redirect to the cart directly.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public static function redirectToCart(array &$form, FormStateInterface $formState): void {
    $formState->setRedirect('commerce_cart.page');
  }

}
