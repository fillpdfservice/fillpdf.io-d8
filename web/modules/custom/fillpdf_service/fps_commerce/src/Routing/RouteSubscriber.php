<?php

namespace Drupal\fps_commerce\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection.
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $cartPage = $collection->get('commerce_cart.page');
    if ($cartPage) {
      $cartPage->setDefault('_controller', '\Drupal\fps_commerce\Controller\CartController::skipCart');
    }
  }

}
