<?php

namespace Drupal\fps_commerce\Override\Plugin\Commerce\TaxType;

use Drupal\commerce_tax\Resolver\ChainTaxRateResolverInterface;
use Drupal\commerce_price\RounderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_tax\Plugin\Commerce\TaxType\EuropeanUnionVat as OriginalEuropeanUnionVat;

/**
 * {@inheritdoc}
 */
class EuropeanUnionVat extends OriginalEuropeanUnionVat {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, RounderInterface $rounder, ChainTaxRateResolverInterface $chain_rate_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $event_dispatcher, $rounder, $chain_rate_resolver);

    $this->taxNumberFieldName = 'field_vat_number';
  }

  /**
   * {@inheritdoc}
   */
  public function apply(OrderInterface $order) {
    $store = $order->getStore();
    $prices_include_tax = $store->get('prices_include_tax')->value;
    $zones = $this->getZones();
    foreach ($order->getItems() as $order_item) {
      $customer_profile = $this->resolveCustomerProfile($order_item);
      if (!$customer_profile) {
        continue;
      }

      $resolved_zones = $this->resolveZones($order_item, $customer_profile);
      $resolved_zones_by_id = [];
      foreach ($resolved_zones as $resolved_zone) {
        $resolved_zones_by_id[$resolved_zone->getId()] = $resolved_zone;
      }
      // Key the resolved zones by id, then merge THAT into $order_item_zones.
      $order_item_zones = $zones + $resolved_zones_by_id;
      $rates = $this->resolveRates($order_item, $customer_profile);
      foreach ($rates as $zone_id => $rate) {
        $zone = $order_item_zones[$zone_id];
        $percentage = $rate->getPercentage();
        // Stores are allowed to enter prices without tax even if they're
        // going to be displayed with tax, and vice-versa.
        // Now that the rates are known, use them to determine the final
        // unit price (which will in turn finalize the order item total).
        if ($prices_include_tax != $this->isDisplayInclusive()) {
          $unit_price = $order_item->getUnitPrice();
          $tax_amount = $percentage->calculateTaxAmount($unit_price, $prices_include_tax);
          $tax_amount = $this->rounder->round($tax_amount);
          if ($prices_include_tax && !$this->isDisplayInclusive()) {
            $unit_price = $unit_price->subtract($tax_amount);
          }
          elseif (!$prices_include_tax && $this->isDisplayInclusive()) {
            $unit_price = $unit_price->add($tax_amount);
          }
          $order_item->setUnitPrice($unit_price);
        }
        // Now determine the tax amount, taking into account other adjustments.
        $adjusted_total_price = $order_item->getAdjustedTotalPrice(['promotion', 'fee']);
        $tax_amount = $percentage->calculateTaxAmount($adjusted_total_price, $this->isDisplayInclusive());
        if ($this->shouldRound()) {
          $tax_amount = $this->rounder->round($tax_amount);
        }

        $order_item->addAdjustment(new Adjustment([
          'type' => 'tax',
          'label' => $zone->getDisplayLabel(),
          'amount' => $tax_amount,
          'percentage' => $percentage->getNumber(),
          'source_id' => $this->entityId . '|' . $zone->getId() . '|' . $rate->getId(),
          'included' => $this->isDisplayInclusive(),
        ]));
      }
    }
  }

}
