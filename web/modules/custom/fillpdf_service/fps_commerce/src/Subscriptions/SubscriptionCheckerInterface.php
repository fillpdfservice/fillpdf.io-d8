<?php

namespace Drupal\fps_commerce\Subscriptions;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Subscription checkers provide information about a user's subscription.
 */
interface SubscriptionCheckerInterface {

  /**
   * Load the user's subscription.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user to get the subscription for.
   *
   * @return \Drupal\commerce_recurring\Entity\SubscriptionInterface|null
   *   The user's subscription.
   */
  public function getUserSubscription(AccountInterface $account): ?SubscriptionInterface;

  /**
   * Retrieve the product the user subscribed to.
   *
   * @param \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription
   *   The subscription.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariation
   *   The product variation associated with the subscription.
   */
  public function getSubscribedProduct(SubscriptionInterface $subscription): ?ProductVariation;

}
