<?php

namespace Drupal\fps_commerce\Subscriptions;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use LogicException;

/**
 * The primary subscription checker implementation.
 */
class SubscriptionChecker implements SubscriptionCheckerInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserSubscription(AccountInterface $account): ?SubscriptionInterface {
    /** @var \Drupal\commerce_recurring\SubscriptionStorageInterface $subscription_storage */
    $subscription_storage = $this->entityTypeManager->getStorage('commerce_subscription');
    $subscription_query = $subscription_storage->getQuery();
    $user_subscriptions = $subscription_query
      ->condition('uid', $account->id())
      ->condition('state', 'active')
      ->condition('type', 'fps_commerce_api_license')
      ->sort('subscription_id', 'DESC')
      ->range(0, 1)
      ->execute();

    if (!$user_subscriptions) {
      return NULL;
    }

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $user_subscription */
    $user_subscription = $subscription_storage->load(reset($user_subscriptions));
    return $user_subscription;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSubscribedProduct(SubscriptionInterface $subscription): ?ProductVariation {
    $purchased_entity_id = $subscription->getPurchasedEntityId();
    if (!$purchased_entity_id) {
      throw new LogicException('Subscription does not contained purchased entity. This is a bug. Cannot proceed.');
    }

    /** @var \Drupal\commerce_product\Entity\ProductVariation $subscription_product */
    $subscription_product = $this->entityTypeManager->getStorage('commerce_product_variation')
      ->load($purchased_entity_id);
    return $subscription_product;
  }

}
