<?php

namespace Drupal\fps_commerce\OrderProcessor;

use CommerceGuys\Addressing\Address;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Applies taxes to orders during the order refresh process.
 */
class ReverseCharge implements OrderProcessorInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    $billing_profile = $order->getBillingProfile();
    // TODO: switch to use the configured taxFieldName.
    if (!isset($billing_profile->field_vat_number)) {
      return;
    }

    $store = $order->getStore();

    $vat_number = $billing_profile->field_vat_number->value;
    if (!$vat_number) {
      return;
    }

    foreach ($order->getItems() as $order_item) {
      // If VAT has not been added, then we don't need to do anything.
      $adjustments = $order_item->getAdjustments();

      // If none of the adjustments are tax adjustments, keep going.
      $has_tax_adjustment = FALSE;
      foreach ($adjustments as $adjustment) {
        if ($adjustment->getLabel() === $this->t('VAT')) {
          $has_tax_adjustment = TRUE;
          break;
        }
      }
      if (!$has_tax_adjustment) {
        continue;
      }

      $customer_address = new Address($billing_profile->address->country_code);
      // Check if the customer's country is the same as one of the store
      // registrations.
      foreach ($store->get('tax_registrations') as $registration) {
        $registration_address = new Address($registration->value);
        if ($registration_address->getCountryCode() === $customer_address->getCountryCode()) {
          // VAT should be collected after all. Note that this an imprecise
          // check, but it's OK because we run later than Commerce's tax order
          // processor. That means that this check won't incorrectly pick up
          // exceptions within the EU that don't have VAT (since we don't try to
          // remove VAT if there is no VAT already on the order).
          return;
        }
      }

      // Have we made it this far? Then it looks like this is a reverse charge.
      // Remove the tax...and can we somehow indicate that it's a reverse
      // charge?
    }
  }

}
