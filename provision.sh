#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ "xxx$1" == "xxx" ]; then
  echo "Usage: ./provision.sh <Drupal VM folder name, 'drupal-vm' or 'production-vm'>"
  exit 1
fi

echo "cd ${DIR}/../$1"
cd ${DIR}/../$1
ANSIBLE_ROLES_PATH=${DIR}/../$1/provisioning/roles:${DIR}/../$1/scripts/roles ANSIBLE_VAULT_PASSWORD_FILE=~/.ansible-vault/d7d8.txt vagrant provision
