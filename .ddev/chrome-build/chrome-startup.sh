#!/bin/bash
if [[ ! -f /.mkcert-configured ]]; then
  #!/usr/bin/env bash
  # Install for root (system).
  CAROOT="/mnt/ddev-global-cache/mkcert" mkcert -install

  # Install for user: chrome.
  su chrome -c 'mkdir -p $HOME/.local/share/mkcert'
  cp -R /mnt/ddev-global-cache/mkcert/* /home/chrome/.local/share/mkcert/
  chown -R chrome: /home/chrome/.local/share/mkcert
  # Create the NSS trust store BEFORE running mkcert; mkcert doesn't reliably
  # work otherwise.
  su chrome -c 'mkdir -p $HOME/.pki/nssdb'
  su chrome -c 'certutil -d sql:$HOME/.pki/nssdb -N --empty-password'
  su chrome -c 'cd $HOME && TRUST_STORES=nss mkcert -install'

  touch /.mkcert-configured
fi

# Run headless Chrome in the foreground (the original container's command).
su chrome -c 'google-chrome --headless --disable-gpu --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222'
