#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
phpunit_config=${PHPUNIT_CONFIG:-${DIR}/../../../phpunit.xml}
php_command=${PHP_COMMAND:-/usr/bin/php}
$php_command "${DIR}"/../../../vendor/bin/phpunit -c "$phpunit_config" --printer="\Drupal\Tests\Listeners\HtmlOutputPrinter" "$@"
