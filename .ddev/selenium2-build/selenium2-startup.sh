#!/bin/bash
if [[ ! -f /.mkcert-configured ]]; then
  #!/usr/bin/env bash
  # Install for root (system).
  sudo CAROOT="/mnt/ddev-global-cache/mkcert" mkcert -install

  # Install for user: seluser.
  mkdir -p "$HOME"/.local/share/mkcert
  sudo cp -R /mnt/ddev-global-cache/mkcert/* /home/seluser/.local/share/mkcert/
  sudo chown -R seluser: /home/seluser/.local/share/mkcert
  # Create the NSS trust store BEFORE running mkcert; mkcert doesn't reliably
  # work otherwise.
  mkdir -p "$HOME"/.pki/nssdb
  certutil -d sql:"$HOME"/.pki/nssdb -N --empty-password
  cd /home/seluser && TRUST_STORES=nss mkcert -install

  sudo touch /.mkcert-configured
fi

/opt/bin/entry_point.sh
