#!/usr/bin/env bash
set -x # echo commands

cd /Users/kevin/vhosts/fillpdf-service/d7d8/docroot/web
lando drush migrate-upgrade --legacy-db-url=mysql://drupal8:drupal8@database/legacy --legacy-root=https://fillpdf.io
