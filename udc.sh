#!/usr/bin/env bash
# Convenience script to update Drupal Commerce. Add --with-dependencies to the
# invocation if desired.
composer update drupal/commerce drupal/commerce_cart drupal/commerce_price drupal/commerce_payment drupal/commerce_order drupal/commerce_product drupal/commerce_store drupal/commerce_checkout drupal/address "$@"
