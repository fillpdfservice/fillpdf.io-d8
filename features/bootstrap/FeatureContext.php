<?php

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_recurring\Entity\BillingSchedule;
use Drupal\commerce_recurring_metered\UsageRecordInterface;
use Drupal\DrupalExtension\Context\MinkContext;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  protected const STORE_ID = 1;

  /**
   * @var string
   */
  protected $screenshotPath;

  /**
   * @var string
   */
  protected $resourcePath;

  /**
   * The base URL from Mink's behat.yml settings.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * @var \Drupal\commerce_store\Entity\StoreInterface[]
   */
  protected $stores = [];

  /**
   * @var \Drupal\commerce_recurring\Entity\BillingScheduleInterface[]
   */
  protected $subscriptionBillingSchedules = [];

  /**
   * @var \Drupal\commerce_product\Entity\ProductInterface[]
   */
  protected $subscriptionProducts = [];

  /**
   * @var \Drupal\commerce_order\Entity\OrderInterface[]
   */
  protected $subscriptionOrders = [];

  /**
   * @var \Drupal\commerce_payment\Entity\PaymentMethodInterface[]
   */
  protected $paymentMethods = [];

  /**
   * @var array
   */
  protected $aliases = [];

  /**
   * @var \Drupal\DrupalExtension\Context\MinkContext
   */
  private $drupalExtensionMinkContext;

  /**
   * The most recent response from the FillPDF Service merge endpoint.
   *
   * @var \GuzzleHttp\Psr7\Response
   */
  private $lastApiMergeResponse;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct($screenshotPath, $resourcePath) {
    $this->screenshotPath = $screenshotPath;
    $this->resourcePath = $resourcePath;
  }

  /**
   * @param \Behat\Behat\Hook\Scope\BeforeScenarioScope $scope
   *
   * @BeforeScenario
   */
  public function gatherContexts(BeforeScenarioScope $scope) {
    /** @var \Behat\Behat\Context\Environment\InitializedContextEnvironment $environment */
    $environment = $scope->getEnvironment();
    $this->drupalExtensionMinkContext = $environment->getContext(MinkContext::class);
    $this->baseUrl = $this->drupalExtensionMinkContext->getMinkParameter('base_url');
  }

  /**
   * @Given /^the store is configured to support subscriptions$/
   */
  public function theStoreIsConfiguredToSupportSubscriptions() {
    throw new PendingException();
  }


  /**
   * @Given I have configured the Drupal FillPDF module
   */
  public function iHaveConfiguredTheDrupalFillpdfModule() {
    throw new PendingException();
  }

  /**
   * @Given FillPDF Service is set up
   */
  public function fillpdfServiceIsSetUp() {
    throw new PendingException();
  }

  /**
   * @When I go to a FillPDF Link provided by the Drupal FillPDF module
   */
  public function iGoToAFillpdfLinkProvidedByTheDrupalFillpdfModule() {
    throw new PendingException();
  }

  /**
   * @Then I get an HTTP response of :arg1
   */
  public function iGetAnHttpResponseOf($arg1) {
    throw new PendingException();
  }

  /**
   * @Then the response data contains a PDF
   */
  public function theResponseDataContainsAPdf() {
    throw new PendingException();
  }

  /**
   * @When I upload a PDF with the Drupal FillPDF module
   */
  public function iUploadAPdfWithTheDrupalFillpdfModule() {
    throw new PendingException();
  }

  /**
   * @Then the response data contains a list of fields
   */
  public function theResponseDataContainsAListOfFields() {
    throw new PendingException();
  }

  /**
   * @Given users are able to subscribe to plans
   */
  public function usersAreAbleToSubscribeToPlans() {
    throw new PendingException();
  }

  /**
   * @When a user subscribes to a plan
   */
  public function aUserSubscribesToAPlan() {
    throw new PendingException();
  }

  /**
   * @Then an email should be sent to the user
   */
  public function anEmailShouldBeSentToTheUser() {
    throw new PendingException();
  }

  /**
   * @Then I should receive an email with the order details
   */
  public function iShouldReceiveAnEmailWithTheOrderDetails() {
    throw new PendingException();
  }

  /**
   * @Given a second user has logged in
   */
  public function aSecondUserHasLoggedIn() {
    throw new PendingException();
  }

  /**
   * @When a user and a second user subscribe to plans
   */
  public function aUserAndASecondUserSubscribeToPlans() {
    throw new PendingException();
  }

  /**
   * @Then the second invoice number is :arg1 higher than the first
   */
  public function theSecondInvoiceNumberIsHigherThanTheFirst($arg1) {
    throw new PendingException();
  }

  /**
   * @Then the order invoice should show the user's VAT number
   */
  public function theOrderInvoiceShouldShowTheUsersVatNumber() {
    throw new PendingException();
  }

  /**
   * @Then the order invoice should show the store's VAT number
   */
  public function theOrderInvoiceShouldShowTheStoresVatNumber() {
    throw new PendingException();
  }

  /**
   * @Given a billing street of :arg1
   */
  public function aBillingStreetOf($arg1) {
    throw new PendingException();
  }

  /**
   * @Given a billing city of :arg1
   */
  public function aBillingCityOf($arg1) {
    throw new PendingException();
  }

  /**
   * @Given a billing postal code of :arg1
   */
  public function aBillingPostalCodeOf($arg1) {
    throw new PendingException();
  }

  /**
   * @Given a billing country of :arg1
   */
  public function aBillingCountryOf($arg1) {
    throw new PendingException();
  }

  /**
   * @Then the order invoice should contain :arg1
   */
  public function theOrderInvoiceShouldContain($arg1) {
    throw new PendingException();
  }

  /**
   * @When a user subscribes to a plan with a valid VAT number
   */
  public function aUserSubscribesToAPlanWithAValidVatNumber() {
    throw new PendingException();
  }

  /**
   * @Given I have set up a monthly billing schedule
   */
  public function iHaveSetUpAMonthlyBillingSchedule() {
    throw new PendingException();
  }

  /**
   * @When I add a new subscription
   */
  public function iAddANewSubscription() {
    throw new PendingException();
  }

  /**
   * @Then I should be able to make the product be for a subscription
   */
  public function iShouldBeAbleToMakeTheProductBeForASubscription() {
    throw new PendingException();
  }

  /**
   * @Then I should be able to set usage limits on the subscription
   */
  public function iShouldBeAbleToSetUsageLimitsOnTheSubscription() {
    throw new PendingException();
  }

  /**
   * @Then I should be able to enable or disable the subscription
   */
  public function iShouldBeAbleToEnableOrDisableTheSubscription() {
    throw new PendingException();
  }

  /**
   * Create the subscription.
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   *   The table values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @Given the following monthly subscription plan exists:
   * @Given the following monthly subscription plans exist:
   */
  public function assertSubscriptions(TableNode $table): void {
    // Delete the test_id billing schedule if it exists. Sometimes it doesn't
    // get cleaned up.
    $this->cleanTestBillingSchedule();

    /** @var \Drupal\commerce_recurring\Entity\BillingScheduleInterface $billing_schedule */
    $billing_schedule = BillingSchedule::create([
      'id' => 'test_id',
      'label' => 'Monthly schedule',
      'displayLabel' => 'Monthly schedule',
      'billingType' => BillingSchedule::BILLING_TYPE_POSTPAID,
      'plugin' => 'rolling',
      'configuration' => [
        'interval' => [
          'number' => '1',
          'unit' => 'month',
        ],
      ],
    ]);
    $billing_schedule->save();
    $this->subscriptionBillingSchedules[] = $billing_schedule;

    // All variations get placed under one product.
    $variations = [];
    foreach ($table->getRows() as $index => $values) {
      if ($index === 0) {
        continue;
      }

      // Create the usage variation.
      $overage_cost = $values[4];
      $usage_variation = ProductVariation::create([
        'type' => 'api_usage',
        'title' => "{$values[0]} Usage",
        'sku' => "{$values[1]}_USAGE",
        'price' => [
          'number' => $overage_cost,
          'currency_code' => 'USD',
        ],
      ]);
      $usage_variation->save();

      $usage_product = Product::create([
        'type' => 'api_usage',
        'title' => $this->getRandom()->name(),
        'stores' => ['online'],
        'variations' => [$usage_variation],
      ]);
      $this->subscriptionProducts[] = $usage_product;

      // Create the subscription variation.
      $service_credits = $values[3];
      $variation = ProductVariation::create([
        'type' => 'default',
        'title' => $values[0],
        'sku' => $values[1],
        'price' => [
          'number' => $values[2],
          'currency_code' => 'USD',
        ],
        'billing_schedule' => $billing_schedule,
        'subscription_type' => [
          'target_plugin_id' => 'fps_commerce_api_license',
        ],
        'license_type' => [
          'target_plugin_id' => 'role',
          'target_plugin_configuration' => [
            'license_role' => 'fillpdf_service',
          ],
        ],
        // Use the unlimited expiry plugin as it's simple.
        'license_expiration' => [
          'target_plugin_id' => 'unlimited',
          'target_plugin_configuration' => [],
        ],
      ]);
      $variation->set('field_metered_usage_product', [
        0 => [
          'target_id' => $usage_variation->id(),
        ],
      ]);
      $variation->set('field_service_credits', [
        0 => $service_credits,
      ]);
      $variation->save();

      $variations[] = $variation;
    }

    $product = Product::create([
      'type' => 'default',
      'title' => $this->getRandom()->name(),
      'stores' => ['online'],
      'variations' => $variations,
    ]);
    $product->save();
    $this->subscriptionProducts[] = $product;

    // Create a /test-pricing menu item for viewing the product.
    $path_alias_storage = Drupal::getContainer()->get('path.alias_storage');
    $alias = $path_alias_storage->save("/product/{$product->id()}", '/test-pricing');
    $this->aliases[] = $alias;

    $current_user = $this->getUserManager()->getCurrentUser();
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = PaymentMethod::create([
      'type' => 'credit_card',
      'payment_gateway' => Drupal::entityTypeManager()
        ->getStorage('commerce_payment_gateway')
        ->load('example'),
      'uid' => $current_user->uid,
      'card_type' => 'visa',
      'card_number' => '4111111111111111',
      'card_exp_month' => '12',
      'card_exp_year' => '2029',
    ]);
    $payment_method->save();
    $this->paymentMethods[] = $payment_method;
  }

  /**
   * @Given the subscription name is :arg1
   */
  public function theSubscriptionNameIs($arg1) {
    throw new PendingException();
  }

  /**
   * @Given the subscription price is :arg1 USD
   */
  public function theSubscriptionPriceIsUsd($arg1) {
    throw new PendingException();
  }

  /**
   * @Given the subscription is enabled
   */
  public function theSubscriptionIsEnabled() {
    throw new PendingException();
  }

  /**
   * @Given the subscription's billing schedule is :arg1
   */
  public function theSubscriptionsBillingScheduleIs($arg1) {
    throw new PendingException();
  }

  /**
   * @When I browse to the Pricing page
   */
  public function iBrowseToThePricingPage() {
    throw new PendingException();
  }

  /**
   * @Then I should see a subscription named :arg1 that costs :arg2 USD
   */
  public function iShouldSeeASubscriptionNamedThatCostsUsd($arg1, $arg2) {
    throw new PendingException();
  }

  /**
   * @Given the subscription is disabled
   */
  public function theSubscriptionIsDisabled() {
    throw new PendingException();
  }

  /**
   * @Then I should not see a subscription named :arg1 that costs :arg2 USD
   */
  public function iShouldNotSeeASubscriptionNamedThatCostsUsd($arg1, $arg2) {
    throw new PendingException();
  }

  /**
   * @Given that I have set up the store :arg1
   */
  public function thatIHaveSetUpTheStore($arg1) {
    // @todo: Fix. No idea why this doesn't work. It returns an object with no values set.
    //    $store_object = \Drupal\commerce_store\Entity\Store::create([
    //      'type' => 'online',
    //      'uid' => 1,
    //      'name' => 'FillPDF Service',
    //      'mail' => 'behat@example.com',
    //      'default_currency' => 'EUR',
    //      'address' => [
    //        'address_line1' => 'Prečna ulica 3',
    //        'locality' => 'Bohinjska Bistrica',
    //        'postal_code' => '4264',
    //        'country_code' => 'SI',
    //      ],
    //    ]);
    //    $store = $this->getDriver()->createEntity('commerce_store', $store_object);
    //
    //    $this->stores[] = $store_object;
  }

  /**
   * @Given that I have set up :arg1 taxes in the store :arg2 for Slovenia
   */
  public function thatIHaveSetUpTaxesInTheStoreForSlovenia($arg1, $arg2) {
    // @todo: Implement actual setup. For now, it's already configured in the DB.
  }

  /**
   * @Given I have added a subscription to the cart
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function iHaveAddedASubscriptionToTheCart() {
    $this->visitPath('/product/1');

    // Record the title so we can ensure it got added.
    $title = $this->getSession()->getPage()->find('css', 'h1 div')->getText();
    $this->getSession()->getPage()->findButton('edit-submit')->click();
    $this->visitPath('/cart');
    $this->assertSession()->pageTextContains($title);
  }

  /**
   * @Then I should see a :arg1 field on the :arg2 form
   */
  public function iShouldSeeAFieldOnTheForm($arg1, $arg2) {
    $payment_information_fieldset = $this->getSession()
      ->getPage()
      ->findById('edit-payment-information');
    Assert::assertEquals($arg2, $payment_information_fieldset->find('css', 'legend span')
      ->getText());
    $this->assertSession()->fieldExists($arg1, $payment_information_fieldset);
  }

  /**
   * @Given I am checking out
   */
  public function iAmCheckingOut() {
    $this->iCheckOut();
  }

  /**
   * @When I check out
   */
  public function iCheckOut() {
    $current_page = parse_url($this->getSession()
      ->getCurrentUrl(), PHP_URL_PATH);
    Assert::assertEquals('/cart', $current_page, 'Currently on /cart page.');
    $this->getSession()->getPage()->findButton('edit-checkout')->click();
    $maybe_checkout = parse_url($this->getSession()
      ->getCurrentUrl(), PHP_URL_PATH);
    Assert::assertStringStartsWith('/checkout', $maybe_checkout, 'Navigation to checkout page successful.');
  }

  /**
   * @Given I have entered a valid VAT number
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function iHaveEnteredAValidVatNumber() {
    $this->getSession()
      ->getPage()
      ->fillField('payment_information[add_payment_method][billing_information][field_vat_number][0][value]', 'ATU71386412');
  }

  /**
   * @Then the VAT should be removed from the order
   * @Then there should be no VAT on the order
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function theVatShouldBeRemovedFromTheOrder(): void {
    $this->assertSession()
      ->elementTextNotContains('css', '.checkout-pane-order-summary .order-total-line__adjustment', 'VAT');
  }

  /**
   * @Given I have entered an invalid VAT number
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function iHaveEnteredAnInvalidVatNumber() {
    $this->getSession()
      ->getPage()
      ->fillField('payment_information[add_payment_method][billing_information][field_vat_number][0][value]', 'ATU71386413');
  }

  /**
   * @Then I should receive an error message informing me that the VAT number
   *   was invalid
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function iShouldReceiveAnErrorMessageInformingMeThatTheVatNumberWasInvalid() {
    $this->assertSession()
      ->elementTextContains('css', '.form-item-payment-information-add-payment-method-billing-information-field-vat-number-0-value', 'The VAT number could not be validated by the European VAT Database.');
  }

  /**
   * @Given /^I am logged in$/
   * @Given /^I have logged in$/
   * @Given /^a user has logged in$/
   * @Given /^a user is logged in$/
   * @throws \Exception
   */
  public function iAmLoggedIn() {
    /** @var \Drupal\Component\Utility\Random $random */
    $random = $this->getRandom();
    $user = (object) [
      'name' => $random->name(),
      'pass' => $random->string(32),
    ];
    $this->userCreate($user);
    $this->login($user);
  }

  /**
   * @Given /^I wait for the form to update$/
   *
   * Simply an alias for waiting for the AJAX to finish that makes more sense
   * in English.
   */
  public function iWaitForTheFormToUpdate() {
    $this->drupalExtensionMinkContext->iWaitForAjaxToFinish();
  }

  /**
   * @Then /^I should see a record in the database containing the time of VAT
   *   number verification$/
   */
  public function iShouldSeeARecordInTheDatabaseContainingTheTimeOfVatNumberVerification(): void {
    // Ensure bootstrapped.
    $this->getDriver();

    // Get the page path we're on and look up the order, then billing profile,
    // then VAT number field value.
    $current_path = parse_url($this->getSession()
      ->getCurrentUrl(), PHP_URL_PATH);
    $order_number = explode('/', ltrim($current_path, '/'))[1];
    $commerce_order = Order::load($order_number);
    $billing_profile = $commerce_order->getBillingProfile();
    $vat_number_field = $billing_profile->field_vat_number->getValue()[0];
    Assert::assertNotEmpty($vat_number_field['timestamp'], 'VAT number field stored a timestamp.');
    Assert::assertNotEmpty($vat_number_field['vies_response'], 'VAT number field stored the VIES response.');
  }

  /**
   * Subscribe the user to the most recently-created subscription variation.
   *
   * @Given I am subscribed to the plan
   * @Given I am subscribed to the first plan
   */
  public function iAmSubscribedToThePlan(): void {
    $variation = end($this->subscriptionProducts)->getVariations()[0];

    // Create an order item, then order creating this.
    $order_item = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $variation,
      'quantity' => '1',
    ]);
    $order_item->save();
    $current_user = $this->getUserManager()->getCurrentUser();
    $initial_order = Order::create([
      'type' => 'default',
      'store_id' => self::STORE_ID,
      'uid' => $current_user->uid,
      'order_items' => [$order_item],
      'state' => 'draft',
      'payment_method' => end($this->paymentMethods),
    ]);
    $initial_order->save();
    $initial_order->getState()->applyTransitionById('place');
    $initial_order->save();

    $this->subscriptionOrders[] = $initial_order;
  }

  /**
   * @When I merge a PDF using the API
   */
  public function mergePdfWithApi(): void {
    $logged_in_user = Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($this->getUserManager()->getCurrentUser()->uid);
    $api_key = $logged_in_user->fps_api_key->value;
    $test_pdf = file_get_contents("{$this->resourcePath}/fillpdf_test_v3.pdf");
    $client = new Client();
    $request_json = \GuzzleHttp\json_encode([
      'api_key' => $api_key,
      'pdf' => base64_encode($test_pdf),
      'fields' => [
        'Text1' => 'test',
      ],
    ]);
    try {
      $this->lastApiMergeResponse = $client->post("{$this->baseUrl}/api/v1/merge", [
        RequestOptions::BODY => $request_json,
        RequestOptions::HEADERS => ['Content-Type' => 'application/json'],
      ]);
    }
    catch (RequestException $re) {
      $this->lastApiMergeResponse = $re->getResponse();
    }
  }

  /**
   * Assert Commerce Recurring Metered usage.
   *
   * @Then my recorded :arg1 usage should be :arg2
   */
  public function assertRecordedUsage($arg1, $arg2): void {
    $entity_type_manager = Drupal::getContainer()
      ->get('entity_type.manager');
    $subscription_storage = $entity_type_manager
      ->getStorage('commerce_subscription');
    $subscription_query = $subscription_storage->getQuery();
    $user_subscriptions = $subscription_query
      ->condition('uid', $this->getUserManager()->getCurrentUser()->uid)
      ->condition('state', 'active')
      ->sort('subscription_id', 'DESC')
      ->range(0, 1)
      ->execute();

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $user_subscription */
    $user_subscription = $subscription_storage->load(reset($user_subscriptions));
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $user_subscription->getCurrentOrder();
    $order_manager = Drupal::getContainer()
      ->get('commerce_recurring.order_manager');
    $order_manager->refreshOrder($order);

    /** @var \Drupal\commerce_recurring\Plugin\Field\FieldType\BillingPeriodItem $field_billing_period */
    $field_billing_period = $order->get('billing_period')->first();
    $usage_proxy = Drupal::getContainer()
      ->get('commerce_recurring_metered.usage_proxy');
    $usage = $usage_proxy->getUsageForPeriod($user_subscription, $field_billing_period->toBillingPeriod());

    $pv_storage = $entity_type_manager->getStorage('commerce_product_variation');
    $subscription_product = $pv_storage
      ->load($user_subscription->getPurchasedEntityId());
    $metered_usage_product_id = (int) $subscription_product->get('field_metered_usage_product')
      ->first()
      ->getValue()['target_id'];

    Assert::assertEquals(
      (int) $arg2,
      array_reduce($usage[$arg1][$metered_usage_product_id], static function ($carry, UsageRecordInterface $item) {
        $carry += $item->getQuantity();
        return $carry;
      }, 0),
      'Recorded usage matches expected usage.');
  }

  /**
   * @Then my order total should be :total :currency_code
   */
  public function assertOrderTotalWithCurrencyCode($total, $currency_code) {
    $this->assertOrderTotal($total, $currency_code);
  }

  /**
   * Ensure order total matches expectation.
   *
   * @param string $total
   *   The expected order total.
   * @param string $currency_code
   *   The currency code.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @Then my order total should be :total
   */
  public function assertOrderTotal($total, $currency_code = 'USD'): void {
    $entity_type_manager = Drupal::getContainer()
      ->get('entity_type.manager');
    $subscription_storage = $entity_type_manager
      ->getStorage('commerce_subscription');
    $subscription_query = $subscription_storage->getQuery();
    $user_subscriptions = $subscription_query
      ->condition('uid', $this->getUserManager()->getCurrentUser()->uid)
      ->condition('state', 'active')
      ->sort('subscription_id', 'DESC')
      ->range(0, 1)
      ->execute();

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $user_subscription */
    $user_subscription = $subscription_storage->load(reset($user_subscriptions));
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $user_subscription->getCurrentOrder();
    $order_manager = Drupal::getContainer()
      ->get('commerce_recurring.order_manager');
    $order_manager->refreshOrder($order);

    Assert::assertEquals(new Price($total, $currency_code), $order->getTotalPrice());
  }

  /**
   * @AfterScenario
   */
  public function cleanup(): void {
    /** @var \Drupal\commerce_store\Entity\Store $store */
    foreach ($this->stores as $store) {
      $this->getDriver()->entityDelete('commerce_store', $store);
    }
    foreach ($this->subscriptionOrders as $order) {
      $order->delete();
    }
    foreach ($this->subscriptionBillingSchedules as $schedule) {
      $schedule->delete();
    }
    foreach ($this->subscriptionProducts as $product) {
      $product->delete();
    }
    foreach ($this->paymentMethods as $paymentMethod) {
      $paymentMethod->delete();
    }
    foreach ($this->aliases as $alias) {
      $path_alias_storage = Drupal::getContainer()->get('path.alias_storage');
      $path_alias_storage->delete(['pid' => $alias['pid']]);
    }
  }

  /**
   * Clean up the test_id billing schedule and stuff using it.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cleanTestBillingSchedule(): void {
    $container = Drupal::getContainer();
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    $storage = $entityTypeManager->getStorage('commerce_subscription');
    $query = $storage
      ->getQuery();
    $subscriptions = $query->condition('billing_schedule', 'test_id')
      ->execute();
    if (!empty($subscriptions)) {
      foreach ($subscriptions as $subscription_stub) {
        $storage->load($subscription_stub)->delete();
      }
    }

    // Then delete the billing schedule if it exists.
    $billing_schedule_storage = $entityTypeManager->getStorage('commerce_billing_schedule');
    try {
      $test_billing_schedule = $billing_schedule_storage->load('test_id');
      if ($test_billing_schedule) {
        $test_billing_schedule->delete();
      }
    }
    catch (Exception $e) {
      // Just ignore exceptions, no big deal.
    }
  }

  /**
   * Check API response code.
   *
   * @param int $code
   *   The status code.
   *
   * @Then the API response code should be :code
   */
  public function assertApiResponseCode($code): void {
    Assert::assertEquals($code, $this->lastApiMergeResponse->getStatusCode(), 'Response code is as expected.');
  }

  /**
   * Check API response contents.
   *
   * @param string $content
   *   The content to match.
   *
   * @Given the API response should contain :content
   */
  public function assertApiResponseContains($content): void {
    Assert::assertNotFalse(strpos((string) $this->lastApiMergeResponse->getBody(), $content), 'Response contains expected message.');
  }

  /**
   * Parse PDF.
   *
   * @When I parse a PDF using the API
   */
  public function parsePdfWithApi(): void {
    $logged_in_user = Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($this->getUserManager()->getCurrentUser()->uid);
    $api_key = $logged_in_user->fps_api_key->value;
    $test_pdf = file_get_contents("{$this->resourcePath}/fillpdf_test_v3.pdf");
    $client = new Client();
    $request_json = \GuzzleHttp\json_encode([
      'api_key' => $api_key,
      'pdf' => base64_encode($test_pdf),
    ]);
    try {
      $this->lastApiMergeResponse = $client->post("{$this->baseUrl}/api/v1/parse", [
        RequestOptions::BODY => $request_json,
        RequestOptions::HEADERS => ['Content-Type' => 'application/json'],
      ]);
    }
    catch (RequestException $re) {
      $this->lastApiMergeResponse = $re->getResponse();
    }
  }

  /**
   * Ensure a subscribe button exists.
   *
   * @Then /^I should see a Subscribe button$/
   */
  public function assertSubscribeButton(): void {
    $dom = $this->getMink()->getSession()->getPage();
    $actions = $dom->find('css', '#edit-actions');
    $subscribe = $actions->find('css', 'input[value="Subscribe"]');
    Assert::assertNotNull($subscribe, 'Subscribe button found.');
  }

  /**
   * Ensure no subscribe button exists.
   *
   * @Then /^I should not see a Subscribe button$/
   */
  public function assertNoSubscribeButton(): void {
    $dom = $this->getMink()->getSession()->getPage();
    $actions = $dom->find('css', '#edit-actions');
    $subscribe = $actions->find('css', 'input[value="Subscribe"]');
    Assert::assertNull($subscribe, 'Subscribe button not found.');
  }

  /**
   * @Then /^I should be on the checkout page$/
   */
  public function assertCheckoutPage(): void {
    $address = $this->getMink()->getSession()->getCurrentUrl();
    // This is good enough for our purposes.
    Assert::assertStringStartsWith("{$this->baseUrl}/checkout/", $address, 'User was redirected to the checkout page.');
  }

}
