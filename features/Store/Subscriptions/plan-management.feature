@api
Feature:
  As a subscriber
  I want to be able to manage my subscription
  So that I only pay for what I need.

  Background:
    Given I am logged in as a user with the "authenticated user" role
    And the following monthly subscription plans exist:
      | Name                | SKU                 | Price | Service credits | Overage cost |
      | Test Subscription   | TEST-SUBSCRIPTION   | 1.00  | 1               | 0.04         |
      | Test Subscription 2 | TEST-SUBSCRIPTION-2 | 2.00  | 10              | 0.03         |
      | Test Subscription 3 | TEST-SUBSCRIPTION-3 | 4.00  | 20              | 0.01         |
    And I am subscribed to the plan

  Scenario: Only one subscription at a time is allowed
    When I go to "/test-pricing"
    Then I should not see a Subscribe button

  Scenario: "Switch Plan" and "Cancel" buttons are shown for non-current and current plan levels, respectively.
    # This is created along with the subscription product.
    When I go to "/test-pricing"
    Then I should not see a Subscribe button
    And I should see the button "Change plan"
    And I should see the button "Cancel"

  @focus
  Scenario: Clicking the Switch Plan button shows an upgrade confirmation screen if the new plan costs more.
    # This is created along with the subscription product.
    Given I am on "/test-pricing"
    When I press "Change plan"
    Then I should see the button "Upgrade"
    And I should see "Your card will be charged $1.00"
    And I should see "Your plan will immediately switch to Test Subscription 2."

  Scenario: When upgrading, the user pays the difference in price.
    # Make sure an order gets created when the user actually clicks the button
    # to upgrade.

  Scenario: Clicking the Switch Plan button shows a downgrade confirmation screen if the new plan costs less. The confirmation screen explains when the new plan will begin (the next billing cycle).

  Scenario: Clicking the Cancel button shows a cancellation confirmation screen.
