@api
Feature: Record PDF usage

  As a store owner
  I want to track API usage by subscribers
  So that I can hold them to their plans.

  If customers go over their plan limit for service credits, they should have to
  upgrade, wait, or start getting charged for additional usage (if they allow
  it).

  Scenario: A merge API call increases the user's usage
    Given I am logged in as a user with the "authenticated user" role and I have the following fields:
      | field_allow_overages | 1 |
    And the following monthly subscription plan exists:
      | Name              | SKU               | Price | Service credits | Overage cost |
      | Test Subscription | TEST-SUBSCRIPTION | 1.00  | 1               | 0.01         |
    And I am subscribed to the plan
    When I merge a PDF using the API
    Then my recorded "counter" usage should be 1
    And my order total should be "1.00"

  # Default is not to allow overages
  Scenario: Cannot exceed limit if overages disabled
    Given I am logged in as a user with the "authenticated user" role
    And the following monthly subscription plan exists:
      | Name              | SKU               | Price | Service credits | Overage cost |
      | Test Subscription | TEST-SUBSCRIPTION | 1.00  | 1               | 0.01         |
    And I am subscribed to the plan
    When I merge a PDF using the API
    And I merge a PDF using the API
    Then the API response code should be 402
    And the API response should contain "This subscription has no remaining service credits."

  Scenario: Can exceed limit (for a charge) if overages enabled
    Given I am logged in as a user with the "authenticated user" role and I have the following fields:
      # This is the user's API key.
      | field_allow_overages | 1 |
    And the following monthly subscription plan exists:
      | Name              | SKU               | Price | Service credits | Overage cost |
      | Test Subscription | TEST-SUBSCRIPTION | 1.00  | 1               | 0.01         |
    And I am subscribed to the plan
    When I merge a PDF using the API
    And I merge a PDF using the API
    Then my recorded "counter" usage should be 2
    And my order total should be "1.01"

  Scenario: A parse API call increases the user's usage
    Given I am logged in as a user with the "authenticated user" role and I have the following fields:
      | field_allow_overages | 1 |
    And the following monthly subscription plan exists:
      | Name              | SKU               | Price | Service credits | Overage cost |
      | Test Subscription | TEST-SUBSCRIPTION | 1.00  | 1               | 0.01         |
    And I am subscribed to the plan
    When I parse a PDF using the API
    Then my recorded "counter" usage should be 1
    And my order total should be "1.00"
