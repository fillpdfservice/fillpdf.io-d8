@api
Feature: Allow subscribing to the service

  As a prospective subscriber
  I want to be able to subscribe to a plan
  So that I can use the API.

  Scenario: A Subscribe button is shown if they are not logged into their user account.
    When I go to "/pricing"
    Then I should see a Subscribe button

  Scenario: A Subscribe button is shown if they are logged in but not subscribed.
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/pricing"
    Then I should see a Subscribe button

  Scenario: Clicking Subscribe takes an anonymous customer to the account registration page
    Given I am on "/pricing"
    When I press the "Subscribe" button
    Then I should be on the checkout page
    And I should not see "FillPDF Service API Key"

  Scenario: Clicking Subscribe takes an authenticated user to the checkout page
    Given I am logged in as a user with the "authenticated user" role
    And I am on "/pricing"
    When I press the "Subscribe" button
    Then I should be on the checkout page

#  # @todo: Implement later. Commerce License already enforces this.
#  Scenario: Only one subscription at a time is allowed
#
#  # @todo: Some basic UI-related stuff (covering essential information about what the license includes)
#
  # @todo: implement later. record-pdf-usage already tests this
#  Scenario: Completing checkout grants them the whatever (role?) associated with the license
