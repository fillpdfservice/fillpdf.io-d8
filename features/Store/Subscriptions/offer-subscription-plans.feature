Feature: Offer plans to subscribe to

  As a store owner
  I want to offer subscription plans
  So that people can sign up and use the service.

  Background:
    Given the store is configured to support subscriptions

  Scenario: Subscriptions can be added
    Given I am logged in as an "administrator"
    Given I have set up a monthly billing schedule
    When I add a new subscription
    Then I should be able to make the product be for a subscription

  Scenario: Usage limits can be set on subscriptions
    Given I am logged in as an "administrator"
    Given I have set up a monthly billing schedule
    When I add a new subscription
    Then I should be able to set usage limits on the subscription

  Scenario: Subscriptions can be enabled and disabled
    Given I am logged in as an "administrator"
    Given I have set up a monthly billing schedule
    When I add a new subscription
    Then I should be able to enable or disable the subscription

  Scenario: Enabled subscriptions show on the Pricing Page
    Given a user has logged in
    And I have set up a monthly billing schedule
    And I have set up a subscription
    And the subscription name is "Clipboard"
    And the subscription price is 6.00 USD
    And the subscription is enabled
    And the subscription's billing schedule is "Monthly"
    When I browse to the Pricing page
    Then I should see a subscription named "Clipboard" that costs 6.00 USD

  Scenario: Disabled subscriptions do not show on the pricing page
    Given a user has logged in
    And I have set up a monthly billing schedule
    And I have set up a subscription
    And the subscription name is "Clipboard"
    And the subscription price is 6.00 USD
    And the subscription is disabled
    And the subscription's billing schedule is "Monthly"
    When I browse to the Pricing page
    Then I should not see a subscription named "Clipboard" that costs 6.00 USD
