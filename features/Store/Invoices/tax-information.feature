Feature: Show tax information on invoice

  As a store owner
  I want to show appropriate tax information on invoices
  So that I am in compliance with local legislation.

  Legislation requires appropriate tax information to be shown, both for the seller and the buyer. Tax-specific information generally encompasses VAT numbers and country-specific "Place of Supply" clauses.

  Background:
    # Make a user
    Given a user is logged in
    # Set up all the plans and stuff
    And users are able to subscribe to plans

  Scenario: Show VAT numbers on the invoice
    When a user subscribes to a plan
    Then the order invoice should show the user's VAT number
    And the order invoice should show the store's VAT number

  Scenario: Place of Supply: To Slovenian customer
    Given a billing street of "Trg svobode 1"
    And a billing city of "Bohinjska Bistrica"
    And a billing postal code of "4264"
    And a billing country of "Slovenia"
    When a user subscribes to a plan
    # @todo: improve the phrasing of this?
    # http://www.mm-konto.si/aktualno/ddv-klavzule/
    Then the order invoice should contain "DDV ni obračunan po prvem odstavku 94. členu ZDDV-1 (nisem identificiran za namene DDV)"

  Scenario: Place of Supply: To EU business customer outside of Slovenia
    Given a billing street of "Ernst-Melchior-Gasse 3"
    And a billing city of "Vienna"
    And a billing postal code of "1020"
    And a billing country of "Austria"
    When a user subscribes to a plan with a valid VAT number
    Then the order invoice should contain "Reverse Charge - VAT exempt under Article 44 Directive 2006/112/ES"

  Scenario: Place of Supply: To EU consumer outside of Slovenia
    Given a billing street of "Ernst-Melchior-Gasse 3"
    And a billing city of "Vienna"
    And a billing postal code of "1020"
    And a billing country of "Austria"
    When a user subscribes to a plan
    Then the order invoice should contain "VAT exempt under Article 287 of Directive"

  # @todo: Norway, Iceland, etc. digital tax if it becomes relevant


