Feature: Invoice numbers are sequential

  As a store owner
  I want to have sequential invoice numbers on Completed orders
  So that I can comply with local legislation.

  Background:
    Given a user has logged in
    Given a second user has logged in
    And users are able to subscribe to plans

  Scenario:
    When a user and a second user subscribe to plans
    Then the second invoice number is 1 higher than the first
