Feature: Create and manage profiles

  As a customer
  I should be able to manage billing profiles
  So that I can save time when buying again.

  Billing profiles save time for customers. If they have to re-subscribe or whatever, it saves them
  time. They don't have to re-enter their address, etc.

  # TODO: flesh out scenarios — was not critical path at time of conception
