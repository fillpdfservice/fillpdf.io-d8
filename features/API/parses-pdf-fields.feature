Feature: Parse PDF fields in PDF

  As a FillPDF Service subscriber using the FillPDF module
  I want my uploaded PDF's fields to be processed
  So that the fields appear in the FillPDF mapping interface.

  Most of the backend functionality here is tested in FillPDF itself, so I only care about the
  simple case of "it works."

  Background:
    Given I have configured the Drupal FillPDF module
    And FillPDF Service is set up

  Scenario: An API call is made to parse a PDF
    When I upload a PDF with the Drupal FillPDF module
    Then I get an HTTP response of "200"
    And the response data contains a list of fields
