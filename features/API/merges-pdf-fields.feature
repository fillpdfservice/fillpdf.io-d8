Feature: Merge PDF fields in PDF

  As a FillPDF Service subscriber using the FillPDF module
  I want to populate an editable PDF with submitted form date and receive a merged PDF back
  So that the functionality will work for me.

  Most of the backend functionality here is tested in FillPDF itself, so I only care about the
  simple case of "it works."

  Background:
    Given I have configured the Drupal FillPDF module
    And FillPDF Service is set up

  Scenario: An API call is made to merge a PDF
    When I go to a FillPDF Link provided by the Drupal FillPDF module
    Then I get an HTTP response of "200"
    And the response data contains a PDF
