Feature: Send email from site

  As a store owner
  I should be able to send email to customers
  So that I can keep them informed of important information.

  This is just to make sure that email sending can be queued up with a fake email provider. I
  obviously can't test with the real email provider, or email will be sent.

  Background:
    # Make a user
    Given a user is logged in
    # Set up all the plans and stuff
    And users are able to subscribe to plans

  Scenario: Send an order email when they order
    When a user subscribes to a plan
    Then an email should be sent to the user

  Scenario: Send the admin an order email when they order
    When a user subscribes to a plan
    Then I should receive an email with the order details
