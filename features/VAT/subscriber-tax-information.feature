@api
Feature: Collect tax information from subscribers

  As a potential subscriber
  I need to be able to provide my VAT number
  so that I can be exempted from paying VAT to the store.

  Cross-border EU supplies require a valid VAT number in order to establish that the customer is a business.

  Background:
    Given that I have set up the store "FillPDF Service"
    And that I have set up "European Union VAT" taxes in the store "FillPDF Service" for Slovenia
    # @todo: Later on, I will probably need to ensure that I'm using the example payment module and not Stripe...although Stripe in test mode might be OK.
    And I am logged in
    And I have added a subscription to the cart

  Scenario: While checking out, a VAT number field is available to enter the number
    When I check out
    Then I should see a "VAT Number" field on the "Payment information" form

  Scenario: If the number is valid, the VAT is removed
    Given I am checking out
    And I fill in "CVV" with "111"
    And I select "Austria" from "Country"
    And I wait for the form to update
    And I fill in "First name" with "Austrian"
    And I fill in "Last name" with "Guy"
    And I fill in "Street address" with "Wiedner Hauptstraße 62"
    And I fill in "City" with "Vienna"
    And I fill in "Postal code" with "1010"
    And I have entered a valid VAT number
    When I press the "Continue to review" button
    Then the VAT should be removed from the order

  Scenario: If the number is invalid, VAT persists
    Given I am checking out
    And I fill in "CVV" with "111"
    And I select "Austria" from "Country"
    And I wait for the form to update
    And I fill in "First name" with "Austrian"
    And I fill in "Last name" with "Guy"
    And I fill in "Street address" with "Wiedner Hauptstraße 62"
    And I fill in "City" with "Vienna"
    And I fill in "Postal code" with "1010"
    And I have entered an invalid VAT number
    When I press the "Continue to review" button
    Then I should receive an error message informing me that the VAT number was invalid
