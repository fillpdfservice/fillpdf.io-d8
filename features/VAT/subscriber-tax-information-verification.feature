@api
Feature: Validate VAT information and save data on that to the DB

  As a store owner
  I need to save information on verification of VAT numbers
  So that I can comply with local legislation.

  We need to save information about when we validated the VAT number for CYA purposes. It's sufficient to just save
  this in the DB somewhere.

  Background:
    Given that I have set up the store "FillPDF Service"
    And that I have set up "European Union VAT" taxes in the store "FillPDF Service" for Slovenia
    # @todo: Later on, I will probably need to ensure that I'm using the example payment module and not Stripe...although Stripe in test mode might be OK.
    And I am logged in
    And I have added a subscription to the cart

  Scenario:
    Given I am checking out
    And I fill in "CVV" with "111"
    And I select "Austria" from "Country"
    And I wait for the form to update
    And I fill in "First name" with "Austrian"
    And I fill in "Last name" with "Guy"
    And I fill in "Street address" with "Wiedner Hauptstraße 62"
    And I fill in "City" with "Vienna"
    And I fill in "Postal code" with "1010"
    And I have entered a valid VAT number
    When I press the "Continue to review" button
    Then I should see a record in the database containing the time of VAT number verification
