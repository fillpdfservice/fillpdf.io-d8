Feature: Charge the correct amount of VAT domestically and abroad.

  As a store owner
  I need to charge appropriate VAT to consumers domestically and in other EU countries
  So that I am in compliance with EU legislation

  Background:
    Given that I have set up the store "FillPDF Service"
    And that I have set up "European Union VAT" taxes in the store "FillPDF Service" for "Slovenia"
    # @todo: Later on, I will probably need to ensure that I'm using the example payment module and not Stripe...although Stripe in test mode might be OK.
    And I am logged in
    And I have added a subscription with a price of 6.99 to the cart

  Scenario: B2C VAT (digital sales under €10,000, not registered domestically for VAT)
    Given the store is not liable for VAT in "Slovenia"
    And digital sales are under the 2019 EU threshold
    And I am checking out
    And I fill in "CVV" with "111"
    And I select "Slovenia" from "Country"
    And I wait for the form to update
    And I fill in "First name" with "Slovenian"
    And I fill in "Last name" with "Guy"
    And I fill in "Street address" with "Prečna ulica 3"
    And I fill in "City" with "Bohinjska Bistrica"
    And I fill in "Postal code" with "4264"
    When I press the "Continue to review" button
    Then there should be no VAT on the order

  Scenario: B2C VAT (digital sales under €10,000, registered domestically for VAT)
    Given the store is liable for VAT in "Slovenia"
    And digital sales are under the 2019 EU threshold
    And I am checking out
    And I fill in "CVV" with "111"
    And I select "Slovenia" from "Country"
    And I wait for the form to update
    And I fill in "First name" with "Slovenian"
    And I fill in "Last name" with "Guy"
    And I fill in "Street address" with "Prečna ulica 3"
    And I fill in "City" with "Bohinjska Bistrica"
    And I fill in "Postal code" with "4264"
    When I press the "Continue to review" button
    Then the VAT on the order should be 1.54

  Scenario: B2C VAT (digital sales over €10,000, registered domestically for VAT)
    Given the store is liable for VAT in "Slovenia"
    And digital sales are over the 2019 EU threshold
    And I am checking out
    And I fill in "CVV" with "111"
    And I select "Austria" from "Country"
    And I wait for the form to update
    And I fill in "First name" with "Austrian"
    And I fill in "Last name" with "Guy"
    And I fill in "Street address" with "Wiedner Hauptstraße 62"
    And I fill in "City" with "Vienna"
    And I fill in "Postal code" with "1010"
    When I press the "Continue to review" button
    Then the VAT on the order should be 1.40
